const csv = require('csv-parser');
const fs = require('fs');
const axios = require('axios');

fs.createReadStream('./data.csv')
  .pipe(csv())
  .on('data', (row) => {
    const mObj = {
      entityId: row['Entity ID'],
      name: row['Name'],
      addressLine1: row['Address > Line 1'],
      addressCity: row['Address > City'],
      addressRegion: row['Address > Region'],
      addressPostalCode: row['Address > Postal Code'],
      mainPhonePhoneNumber: row['Main Phone > Phone Number'],
      prominentBrandinde: row['Prominent Brand'],
    };
    const JSONobj = JSON.stringify(mObj);
    console.log(JSONobj);
    axios.post('http://3.135.62.217:1337/locations', mObj).then(res => {
      console.log('SENT');
    })
    .catch(error => {
      console.log(error);
    });
  })
  .on('end', () => {
    console.log('FINISHED');
  });
